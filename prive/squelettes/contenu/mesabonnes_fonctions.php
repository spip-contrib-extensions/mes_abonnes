<?php

/**
 * Filtre pour générer un lien transmettre
 *
 * le fichier est protegé par un hash de faible sécurité
 *
 * @param string $op
 * @param array $args
 * @param string $lang
 * @param string $title
 * @return string
 *     Code HTML du lien
 *@uses generer_url_api_low_sec()
 * @example
 *     - `[(#VAL{a_suivre}|bouton_mesabonnes)]`
 *     - `[(#VAL{signatures}|bouton_spip_rss{#ARRAY{id_article,#ID_ARTICLE}})]`
 *
 * @filtre
 */
function bouton_mesabonnes($op, $title='inconnu') {
	include_spip('inc/acces');
	$args = [];

	$title = $op == 'mesabonnes.csv'?  _T('mesabonnes:export_abonnes_csv') : _T('mesabonnes:export_abonnes_csv_bulk');

	$url = generer_url_api_low_sec('transmettre', 'csv', $op, '', http_build_query($args), false, true);
	return "<a href='$url'>$title</a>";
}

